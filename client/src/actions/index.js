import * as types from '../constants/ActionTypes';

export const addNews = text => ({type: types.ADD_NEWS, text});