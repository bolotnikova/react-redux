import React, { Component } from 'react';
import './signUp.css';
import Header from '../Header/Header';
import {FormName, FormMail, FormPassword} from '../../components/Form';
import axios from 'axios';



class News extends Component {
  constructor(props) {
    super(props);
    this.state = {
      name:'',
      email:'',
      password:'',
      error: '',
      success: false
    };

    this.handleNameChange = this.handleNameChange.bind(this);
    this.handleEmailChange = this.handleEmailChange.bind(this);
    this.handlePasswordChange = this.handlePasswordChange.bind(this);
  }

  handleNameChange(e){
    this.setState({name:e.target.value.trim()})
  }

  handleEmailChange(e){
    this.setState({email:e.target.value.trim()})
  }

  handlePasswordChange(e){
    this.setState({password:e.target.value})
  }

  signUp() {
    fetch('/auth/signup', {
      method: 'POST',
      headers: {
        'Accept': 'application/json, text/plain, */*',
        'Content-Type': 'application/json'
      },
      body: JSON.stringify(this.state)
    })
      .then(prev => prev.json())
      .then(result => {
        if (result.success) {
          this.setState({
            success: true,
          })
        } else {
          this.setState({
            error: result.message
          });
          setTimeout(() => this.setState({error: ''}), 5000)
        }
      });
  }

  render() {
    let block = (this.state.success)
      ? (<p className='sign-up-success'>Registration completed successfully!</p>)
      : (
        <div className='sign-up'>
          <FormName
            value={this.state.name}
            onChange={this.handleNameChange}
          />
          <FormMail
            error={this.state.error}
            value={this.state.email}
            onChange={this.handleEmailChange}
          />
          <FormPassword
            value={this.state.password}
            onChange={this.handlePasswordChange}
          />
          <button
            onClick={() => this.signUp()}
          >Sign up</button>
        </div>
      );
    return (
      <div>
        <Header/>
        {block}
        <a href="/auth/github">GitHub</a>
        <a href="/auth/google">Sign In with Google</a>
      </div>
    );
  }
}

export default News;