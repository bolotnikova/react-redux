const express = require('express');
const router = express.Router();
const auth = require('../controlers/auth');
const passport = require('passport');

router.post('/signup', auth.signUp);

router.post('/login',  passport.authenticate('local'), auth.logIn);

router.post('/login/github',  passport.authenticate('github'), auth.logIn);




router.get('/github',
  passport.authenticate('github', {successRedirect: '/'}));


router.get('/github/callback',
  passport.authenticate('github', {
    failureRedirect: '/'
  }),
  function (req, res) {
    console.log('user', req.user);
    res.redirect('/user/'+req.user._id);
  });



router.get('/google',
  passport.authenticate('google', { scope: ['profile'] }));

router.get('/google/callback',
  passport.authenticate('google', { failureRedirect: '/' }),
  function (req, res) {
    console.log('user', req.user);
    res.redirect('/user/'+req.user._id);
  });

module.exports = router;