const passport       = require('passport');
const LocalStrategy  = require('passport-local').Strategy;
const User = require('./models/users');
const JwtStrategy = require('passport-jwt').Strategy,
  ExtractJwt = require('passport-jwt').ExtractJwt;
const GoogleStrategy = require('passport-google-oauth20').Strategy;
var GithubStrategy = require('passport-github').Strategy;

const GOOGLE_CLIENT_ID = "768442762938-qp0vl8369fk9k2otrrg4ja5r41vit58u.apps.googleusercontent.com";
const GOOGLE_CLIENT_SECRET = "UItI6nO9XWtv6Jd0HzGeeEK2";



const opts = {};
opts.jwtFromRequest = ExtractJwt.fromAuthHeaderAsBearerToken();
opts.secretOrKey = 'secret';


passport.use(new LocalStrategy({
    usernameField: 'email',
    passwordField: 'password'
  },
  function(email, password, done) {
    User.findOne({ email: email }, function(err, user) {
      if (err) { return done(err); }
      if (!user) {
        return done(null, false, { message: 'Incorrect username.' });
      }
      if (!user.validPassword(password)) {
        return done(null, false, { message: 'Incorrect password.' });
      }
      return done(null, user);
    });
  }
));

passport.serializeUser(function(user, done) {
  done(null, user.id);
});


passport.deserializeUser(function(id, done) {
  User.findById(id, function(err,user){
    err
      ? done(err)
      : done(null,user);
  });
});

passport.use(new JwtStrategy(opts, function(jwt_payload, done) {
  User.findOne({_id: jwt_payload.id}, function(err, user) {
    if (err) {
      return done(err, false);
    }
    if (user) {
      return done(null, user);
    } else {
      return done(null, false);
      // or you could create a new account
    }
  });
}));

passport.use(new GoogleStrategy({
    clientID: GOOGLE_CLIENT_ID,
    clientSecret: GOOGLE_CLIENT_SECRET,
    callbackURL: "http://localhost:3001/auth/google/callback"
  },
  function(accessToken, refreshToken, profile, cb) {
    // console.log('avatar: ', profile.photos[0].value);
    User.findOne({GoogleId: profile.id}, function (err, user) {
      if (user === null) {
        let newUser = new User({
          name: profile.name.givenName,
          surname: profile.name.familyName,
          avatar: profile.photos[0].value,
          GoogleId: profile.id,
        });
        return newUser.save(function (err, user) {
          return cb(err, user);
        })
      }
      return cb(err, user);
    })
  }
));

passport.use(new GithubStrategy({
    clientID: "de416350691a31846416",
    clientSecret: "809c434f559e98e4fb42285a2748cf8b14fafaba",
    callbackURL: "http://localhost:3001/auth/github/callback"
  },
  function(accessToken, refreshToken, profile, cb) {
    User.findOne({GitHubId: profile.id}, function (err, user) {
      if (user === null) {
        let newUser = new User({
          name: profile.username,
          avatar: profile.photos[0].value,
          GitHubId: profile.id,
        });
        return newUser.save(function (err, user) {
          return cb(err, user);
        })
      }
      return cb(err, user);
    })
  }
));

module.exports = passport;