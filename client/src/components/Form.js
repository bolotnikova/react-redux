import React from 'react';

export const FormFile = (props) => {
  return (
    <label>
      Avatar:
      <input
        type='file'
        value={props.value}
        onChange={props.onChange}
      />
    </label>
  )
};

export const FormName = (props) => {
  return (
    <label>
      {props.label}
      <input
        type='text'
        value={props.value}
        onChange={props.onChange}
      />
    </label>
  )
};

export const FormMail = (props) => {
  return (
    <label>
      email:
      <input
        type='mail'
        name="email"
        value={props.value}
        onChange={props.onChange}
      />
      <span className='error'>{props.error}</span>
    </label>
  )
};

export const FormPassword = (props) => {
  return (
    <label>
      Password:
      <input
        type='password'
        name="password"
        value={props.value}
        onChange={props.onChange}
      />
    </label>
  )
};

export const FormTitle = (props) => {
  return (
    <label>
      Title:
      <input
        type='text'
        value={props.value}
        onChange={props.onChange}
      />
    </label>
  )
};

export const FormDate = (props) => {
  return (
    <label>
      Date:
      <input
        type='date'
        min='1900-01-01'
        max='3000-01-01'
        value={props.value}
        onChange={props.onChange}
      />
    </label>
  )
};

export const FormText = (props) => {
  return (
    <div>
      <label>
        Text:
      </label>
      <textarea
        rows={'7'}
        value={props.value}
        onChange={props.onChange}
      />
    </div>
  )
};