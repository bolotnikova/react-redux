import React, {Component} from 'react';
import './news.css'

class News extends Component {
  constructor(props) {
    super(props);
    this.state = {
      allText: false,
    }
  }

  changeView(){
    this.setState({allText: !this.state.allText})
  }

  render() {
    let text = this.props.news.text;
    let allText = this.state.allText;

    let FullNews = allText
      ? () => (<div>
          <p>{text}</p>
          <button onClick={() =>this.changeView()}>hide news</button>
        </div>)
      : () => (<div>
        <p>{text.substr(0, 50)}...</p>
        <button onClick={() =>this.changeView()}>full news</button>
      </div>);

    return (
      <article className='news-item'>
        <h3>
          {this.props.news.title}
          <span>{this.props.news.date}</span>
        </h3>
        {(text.length > 50)
          ? <FullNews/>
          : <p className='news-text'>{text}</p>
        }
      </article>
    )
  }
}

export default News;