const News = require('../models/news');

function getAll(req, res) {
  return News.find()
    .then((news) => {
      return res.json({success: true, news: news});
    })
    .catch((err) => {
      console.error(err.message);
      return res.json({
        success: false,
        message: 'News not found. Please try again.'
      });
    });
}

function getById(req,res) {
  News.find({userId: req.params.id})
    .then(news => res.json({success: true, news: news}))
    .catch(err => {
      console.error(err.message);
      return res.json({
        success: false,
        message: 'News not found. Please try again.'
      });
    });
}

function add(req, res) {
  const {title, text, date, userId} = req.body;
  let news = {};
  if(title && text && date){
    news = new News({
      userId: userId,
      title: title,
      text: text,
      date: date,
    });
  }

  return news.save()
    .then((news) => {
      return res.json({success: true, news: news});
    })
    .catch((err) => {
      console.error(err.message);
      return res.json({
        success: false,
        message: 'Server can`t save news. Please try later.'
      })
    });
}

module.exports.getAll = getAll;
module.exports.getById = getById;
module.exports.add = add;