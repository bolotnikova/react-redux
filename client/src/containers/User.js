import React, { Component } from 'react';
import Header from './Header/Header';
import {UserData} from './UserData';
import UserUpdate from './UserUpdate';
import UserNews from './UserNews';
import AddNews from './AddNews/AddNews';
import './user.css';



class User extends Component {
  constructor(props) {
    super(props);
    this.state = {
      user: {},
      news: [],
      popup: false,
      update: false,
      auth: JSON.parse(localStorage.getItem('user')) || '',
    };
  }

  componentDidMount() {
    fetch('/users/'+this.props.match.params.id,)
      .then(prev => prev.json())
      .then(result => {
        if (result.success) {
          this.setState({
            user: result.user,
          });
        }
      });
    fetch('/news/'+this.props.match.params.id,)
      .then(prev => prev.json())
      .then(result => {
        if (result.success) {
          this.setState({
            news: result.news,
          });
        }
      });
  }

  showPopup() {
    this.setState({
      popup: true,
    });
  }

  hidePopup() {
    this.setState({
      popup: false,
    });
  }

  addNews(item) {
    let date = new Date();
    item.date = date.toLocaleString();
    item.userId = this.state.user._id;

    fetch('/news/'+this.props.match.params.id, {
      method: 'PUT',
      headers: {
        'Authorization': localStorage.getItem('token'),
        'Accept': 'application/json, text/plain, */*',
        'Content-Type': 'application/json'
      },
      body: JSON.stringify(item)
    })
      .then(prev => {
        if (prev.status === 401) {
          this.authErr();
        }
        if (prev.status === 200) {return prev.json()}
      })
      .then(result => {
        if (result.success) {
          this.state.news.push(result.news);
          this.setState({
            news: this.state.news,
            popup: false,
          });
        } else {
          this.setState({
            error: result.message
          });
        }
      })
      .catch(console.log);
  }

  changeUpdate() {
    this.setState({
      update: !this.state.update,
    });
  }

  updateUser(item) {
    let user = this.state.user;
    let data = new FormData();
    data.append('avatar', item.avatar[0]);
    data.append('name', item.name);
    data.append('surname', item.surname);
    data.append('date', item.date);

    fetch('/users/', {
        method: 'PUT',
        headers: {
          'Authorization': localStorage.getItem('token'),
        },
        body: data
      })
        .then(prev => {
          if (prev.status === 401) {
            this.authErr();
          }
          if (prev.status === 200) {return prev.json()}
        })
        .then(result => {
          if (result.success) {
            user.name = item.name;
            user.surname = item.surname;
            user.date = item.date;
            user.avatar = item.avatar;
            this.setState({
              user: user,
              update: false,
            });
          } else {
            this.setState({
              error: result.message
            });
          }
        });
  }

  authErr() {
    localStorage.removeItem('user');
    window.location.assign('http://localhost:3000/login');
  }

  logOut() {
    localStorage.removeItem('user');
    this.setState({auth: ''});
  }

  render() {
    let condition = (this.state.auth && this.state.auth._id === this.state.user._id);
    return (
      <div>
        <Header
          user={this.state.auth}
          onExit={() => this.logOut()}/>
        <div className='user container'>
          {this.state.update
            ? <UserUpdate
              user={this.state.user}
              onExit={() => this.changeUpdate()}
              onSave={(item) => this.updateUser(item)}
            />
            : <UserData
              auth={condition}
              user={this.state.user}
              onUpdate={() => this.changeUpdate()}
            />
          }
          <hr />
          {
            condition
              ? <button onClick={() => this.showPopup()}>Add news</button>
              : null
          }
          <UserNews news={this.state.news}/>
          {this.state.popup
            ? <AddNews
              onExit={() => this.hidePopup()}
              onAddNews={(item) => this.addNews(item)}
            />
            : null
          }
        </div>

      </div>
    );
  }
}

export default User;