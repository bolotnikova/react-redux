const express = require('express');
const router = express.Router();
const news = require('../controlers/news');
const passport = require('passport');


/* GET home page. */
router.get('/', news.getAll);

router.get('/:id', news.getById);

router.put('/:id', passport.authenticate('jwt', {session:false}), news.add);

module.exports = router;
