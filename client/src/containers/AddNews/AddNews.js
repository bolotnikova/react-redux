import React, { Component } from 'react';
import './addNews.css';
import {FormTitle, FormText} from '../../components/Form';



class Login extends Component {
  constructor(props) {
    super(props);
    this.state = {
      title: '',
      text: '',
    };

    this.handleTitleChange = this.handleTitleChange.bind(this);
    this.handleTextChange = this.handleTextChange.bind(this);

  }

  handleTitleChange(e){
    this.setState({title:e.target.value.trim()})
  }

  handleTextChange(e){
    this.setState({text:e.target.value})
  }

  render() {
    return (
      <div>
        <div className='back' />
        <div className='login'>
          <button
            className='btn-close'
            onClick={this.props.onExit}
          >X</button>
          <FormTitle
            value={this.state.title}
            onChange={this.handleTitleChange}
          />
          <FormText
            value={this.state.text}
            onChange={this.handleTextChange}
          />
          <button onClick={() => this.props.onAddNews(this.state)}>Add</button>
          <span className='error'>{this.state.error}</span>
        </div>
      </div>
    );
  }
}

export default Login;