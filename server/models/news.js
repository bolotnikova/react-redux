let mongoose = require('mongoose');


let newsSchema = mongoose.Schema({
  userId: String,
  title: String,
  text: String,
  date: String,
  tags: Array,
});


let News = mongoose.model('News', newsSchema);

module.exports = News;