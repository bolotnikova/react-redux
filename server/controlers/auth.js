const User = require('../models/users');
const jwt = require('jsonwebtoken');

function signUp(req, res) {
  const name = req.body.name;
  const email = req.body.email;
  const password = req.body.password;
  let newUser = {};

  if(name && email && password){
    newUser = new User({
      name: name,
      email: email,
      password: password,
    });
  }

  if (!newUser) {
    console.error('Error: newUser');
    return res.json({
      success: false,
      message: 'User data not found. Please try again.'
    });
  }
  validateSignUp(email, function(result) {
    if (result) {
      return newUser.save()
        .then((user) => {
          return res.json({success: true, id: user._id});
        })
        .catch((err) => {
          console.error(err.message);
          return res.json({
            success: false,
            message: 'Server can`t save user. Please try later.'
          })
        });
    } else {
      console.error('Error: User');
      return res.json({
        success: false,
        message: 'User already registered.'
      });
    }
  });
}

function validateSignUp(username, callback){
  User.findOne({email: username
  }, function(err, result){
    if(result==null){
      callback(true)
    }
    else{
      callback(false)
    }
  });
}

function logIn(req, res) {
  const user = req.user;
  const token = jwt.sign({
    id: user._id,
  }, 'secret', { expiresIn: '1h' });
  if(user) {
    return res.json({success: true, user: user, token: 'Bearer ' + token});
  } else {
    console.error('Error: User');
    return res.json({
      success: false,
      message: 'Wrong email or password.'
    });
  }
}

function logInGitHub(req, res) {
  User.findOne({GitHubId: req.user.id})
    .then(user => {
      const token = jwt.sign({
        id: user._id,
      }, 'secret', { expiresIn: '1h' });
      return res.json({success: true, user: user, token: 'Bearer ' + token});
    })
    .catch(console.log);
}


module.exports.signUp = signUp;
module.exports.logIn = logIn;
module.exports.logInGitHub = logInGitHub;
