import React, { Component } from 'react';
import './login.css';
import {Link} from 'react-router-dom';
import {FormMail, FormPassword} from '../../components/Form';



class Login extends Component {
  constructor(props) {
    super(props);
    this.state = {
      email: '',
      password: '',
      error: '',
    };

    this.handleEmailChange = this.handleEmailChange.bind(this);
    this.handlePasswordChange = this.handlePasswordChange.bind(this);
  }

  handleEmailChange(e){
    this.setState({email:e.target.value.trim()})
  }

  handlePasswordChange(e){
    this.setState({password:e.target.value})
  }

  logIn() {
    fetch('/auth/login', {
      method: 'POST',
      headers: {
        'Accept': 'application/json, text/plain, */*',
        'Content-Type': 'application/json'
      },
      body: JSON.stringify(this.state)
    })
      .then(prev => {
        if (prev.status === 200) {return prev.json()}
        if (prev.status === 401) {return {success: false, message: 'Wrong email or password.'}}
      })
      .then(result => {
        if (result.success) {
          localStorage.setItem('user', JSON.stringify(result.user));
          localStorage.setItem('token', result.token);
          window.location.assign('http://localhost:3001/user/'+result.user._id)
        } else {
          this.setState({
          error: result.message
          });
        }
      })
      .catch(err => {
        console.log(err);
      // this.setState({
      //   error: err,
      // });
    })
  }

  render() {
    return (
      <div>
        <div className='back' />
        <div className='login'>
          <button
            className='btn-close'
            onClick={this.props.onExit}
          >X</button>
          <div className='form-group'>
            <FormMail
              value={this.state.email}
              onChange={this.handleEmailChange}
            />
            <FormPassword
              value={this.state.password}
              onChange={this.handlePasswordChange}
            />
          </div>
          <button onClick={() => this.logIn()}>Login</button>
          <button><Link to="/signup">Sign up</Link></button>
          <span className='error'>{this.state.error}</span>
        </div>
      </div>
    );
  }
}

export default Login;