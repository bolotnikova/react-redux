const express = require('express');
const router = express.Router();
const users = require('../controlers/users');
const passport = require('passport');
const multer  = require('multer');
const upload = multer({ dest: '../uploads/avatars/' });

/* GET users listing. */
router.get('/:id', users.getById);

var cpUpload = upload.fields([{ name: 'avatar', maxCount: 1}]);

router.put('/', passport.authenticate('jwt', {session:false}), upload.single('avatar'), users.update);


module.exports = router;
