import React, {Component} from 'react';
import {Link} from 'react-router-dom';
import Login from '../Login/Login';
import './header.css';

class Header extends Component {
  constructor(props) {
    super(props);
    this.state = {
      popup: false,
      user: this.props.user,
    }
  }

  componentWillReceiveProps(nextProps) {
    this.setState({user: nextProps.user})
  }

  showPopup() {
    this.setState({
      popup: true,
    });
  }

  hidePopup() {
    this.setState({
      popup: false,
    });
  }

  render() {
    const user = this.state.user;
    return (
      <div className='header'>
        <div className='container header-container'>
          <menu>
            <li><Link to="/">All news</Link></li>
            {
              user
                ? <li><Link to={"/user/"+user._id}>{user.name}</Link></li>
                : null
            }

          </menu>
          {
            user
              ? <button
                  onClick={() => this.props.onExit()}
                >Log out</button>
              : <div className='header-btn'>
                  <button
                    onClick={() => this.showPopup()}
                  >Login</button>
                  <button><Link to="/signup">Sign up</Link></button>
                </div>
          }
        </div>
        {this.state.popup
          ? <Login
            onExit={() => this.hidePopup()}/>
          : null
        }
      </div>
    )
  }
}

export default Header;