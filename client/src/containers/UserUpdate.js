import React, { Component } from 'react';
import {FormDate, FormName, FormFile} from "../components/Form";


class UserUpdate extends Component {
  constructor(props) {
    super(props);
    this.state = {
      name: this.props.user.name,
      surname: this.props.user.surname,
      date: this.props.user.date,
      avatar: '',
    };

    this.handleFileChange = this.handleFileChange.bind(this);
    this.handleNameChange = this.handleNameChange.bind(this);
    this.handleSurnameChange = this.handleSurnameChange.bind(this);
    this.handleDateChange = this.handleDateChange.bind(this);
  }

  handleFileChange(e){
    this.setState({avatar:e.target.files});
    console.log('avatar', this.state.avatar);
  }

  handleNameChange(e){
    this.setState({name:e.target.value.trim()})
  }

  handleSurnameChange(e){
    this.setState({surname:e.target.value})
  }

  handleDateChange(e){
    this.setState({date:e.target.value})
  }

  render() {
    return (
      <div className='user-update'>
        <div>
          <button
            className='btn-close'
            onClick={this.props.onExit}
          >X</button>
          <div className='form-group'>
            <FormFile
              // value={this.state.avatar}
              onChange={this.handleFileChange}
            />
            <FormName
              label={'Name: '}
              value={this.state.name}
              onChange={this.handleNameChange}
            />
            <FormName
              label={'Last name: '}
              value={this.state.surname}
              onChange={this.handleSurnameChange}
            />
            <FormDate
              value={this.state.date}
              onChange={this.handleDateChange}
            />
          </div>
          <button onClick={() => this.props.onSave(this.state)}>Save</button>
        </div>
      </div>
    );
  }
}

export default UserUpdate;