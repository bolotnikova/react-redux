let mongoose = require('mongoose');


let userSchema = mongoose.Schema({
  name: String,
  surname: String,
  date: String,
  email: String,
  password: String,
  avatar: String,
  GitHubId: String,
  GoogleId: String,
});

userSchema.methods.validPassword = function (password) {
  return (this.password === password);
};

let User = mongoose.model('User', userSchema);

module.exports = User;