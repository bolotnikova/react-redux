import React from 'react';
import {
  BrowserRouter as Router,
  Route
} from 'react-router-dom';
// import App from './App';
import News from './containers/AllNews';
import User from './containers/User';
import SignUp from './containers/SignUp/SignUp';
import LoginPage from './containers/Login/LoginPage';

import './index.css';

const Routes = () => (
  <Router>
    <div>
      <Route exact path="/" component={News}/>
      <Route path="/signup" component={SignUp}/>
      <Route path="/login" component={LoginPage}/>
      <Route exact path="/user/:id" component={User}/>
    </div>
  </Router>
);

export default Routes;
