import React, { Component } from 'react';
import News from '../components/News'



class UserNews extends Component {
  render() {
    return (
      <div className='user-news'>
        {
          (this.props.news === undefined || this.props.news.length === 0)
          ? <p>Write your first news</p>
          : (<div className='news'>
              {this.props.news.map(news => {
                return (
                  <div key={news._id}>
                    <News news={news}/>
                  </div>
                  )
              })}
            </div>)
        }
      </div>
    );
  }
}

export default UserNews;

