import React from 'react';



export const UserData = (props) => {
  console.log('user', props.user);
  return (
    <div className='user-data'>
      <div>
        <div className='avatar'>
          <img src={props.user.avatar} alt='upload avatar'/>
        </div>
        <p>Name: {props.user.name}</p>
        <p>Last name: {props.user.surname==='surname' ? '' : props.user.surname}</p>
        <p>Date: {props.user.date}</p>
        {
          props.auth
            ? <button onClick={props.onUpdate}>update data</button>
            : null
        }
      </div>
    </div>
  );

};