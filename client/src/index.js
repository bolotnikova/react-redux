import React from 'react'
import ReactDOM from 'react-dom'
// import {AppContainer} from 'react-hot-loader'
import Routes from './routes'

ReactDOM.render(
  <Routes />,
  document.getElementById('root')
);
// registerServiceWorker();
