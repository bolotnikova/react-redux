import React, { Component } from 'react';

class Title extends Component {
  constructor(props) {
    super(props);
    this.state = {
      message: 'Loading...',
    };
  }

  componentDidMount() {
    fetch('users/')
      .then((prev) => {
        console.log(prev);
        return prev.json();
      })
      .then((res) => {
        this.setState({message: res.message});
      });
  }

  render() {
    return <h1>{this.state.message}</h1>
  }
}

export default Title;