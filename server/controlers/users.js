const User = require('../models/users');

function getById(req,res) {
  User.findOne({_id: req.params.id})
    .then(user => res.json({success: true, user: user}))
    .catch(err => {
      console.error(err.message);
      return res.json({
        success: false,
        message: 'News not found. Please try again.'
      });
    });
}

function update(req, res) {
  console.log('file: ', req.file.path);
  return User.update({_id: req.user._id}, {
    name: req.body.name,
    surname: req.body.surname || '',
    date: req.body.date || '',
    avatar: req.file.path || '',
  })
    .then(() => {
      return res.json({success: true});
    })
    .catch((err) => {
      console.error(err.message);
      return res.json({
        success: false,
        message: 'Server can`t add news. Please try later.'
      });
    });
}

module.exports.getById = getById;
module.exports.update = update;