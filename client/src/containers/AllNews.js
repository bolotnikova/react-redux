import React, { Component } from 'react';
import Header from './Header/Header';
import UserNews from './UserNews';



class News extends Component {
  constructor(props) {
    super(props);
    this.state = {
      news: [],
      auth: JSON.parse(localStorage.getItem('user')),
    };
  }

  componentDidMount() {
    fetch('/news')
      .then(prev => prev.json())
      .then(result => {

        this.setState({
          news: result.news,
        })
      })
      .catch(console.log);
  }

  logOut() {
    localStorage.removeItem('user');
    this.setState({auth: ''});
  }

  render() {
    return (
      <div className='news'>
        <Header
          user={this.state.auth}
          onExit={() => this.logOut()}/>
        <div className='container'>
          <UserNews news={this.state.news}/>
        </div>
      </div>
    );
  }
}

export default News;